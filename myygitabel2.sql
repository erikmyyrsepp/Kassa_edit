﻿CREATE TABLE Myygid2
(
	kood INT IDENTITY PRIMARY KEY,
	aeg DATETIME,
	tekst NVARCHAR(200),
	admin NVARCHAR(32),
	Tsekinr CHAR(12),
	Kaubakood INT,
	Kogus DECIMAL (7,2),
	Hind DECIMAL (7,2),
	VatPercent DECIMAL(5,2),
	Summa DECIMAL(7,2),
	Sula DECIMAL(7,2),
	Kaart DECIMAL(7,2)

)