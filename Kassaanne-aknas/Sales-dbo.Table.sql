﻿CREATE TABLE [dbo].[Myygid] (
    [Aeg]		    ROWVERSION    NOT NULL,
	[TsekkNr]		NVARCHAR(50) NOT NULL, 
    [Maksja]        INT           NULL,
    [Tootekood]     NCHAR (10)    NOT NULL,
    [Toote nimetus] NVARCHAR (50) NOT NULL,
    [Kogus]         INT           NOT NULL,
	[Hind]			DECIMAL NOT NULL, 
    [KM määr]	    DECIMAL NOT NULL, 
    [Summa]         DECIMAL (18)  NOT NULL,
    [Sula]			DECIMAL NOT NULL, 
    [Kaart]			DECIMAL NOT NULL, 
	[Müüja]         NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([Aeg] ASC),
    CONSTRAINT [FK_Myygid_Myygid] FOREIGN KEY ([Aeg]) REFERENCES [dbo].[Myygid] ([Aeg])
);

