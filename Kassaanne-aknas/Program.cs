﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Kassaanne_aknas
{
    public class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public void Main()
        {
            #region Anndmebaasi lugemine
            /* 
                if (MessageBox.Show("Kas loen andmebaasi", "Küsimus", MessageBoxButtons.YesNo)==DialogResult.Yes && false)
                {
                    SalesDBEntities db = new SalesDBEntities();
                    string[] read = File.ReadAllLines(@"C:\Users\erikm\Desktop\Ülesanne\Myygiaruanne.csv");

                    foreach (var rida in read.Skip(1))
                    {
                        string[] veerud = rida.Split(';');
                        Myygid2 m2 = null;

                        db.Myygid2.Add(m2 = new Myygid2
                        {
                            aeg = DateTime.Parse(veerud[0]),
                            tekst = veerud[1],
                            admin = veerud[2],
                            Tsekinr = veerud[3],
                            Kaubakood = int.Parse(veerud[4]),
                            Kogus = decimal.Parse(veerud[5]),
                            Hind = decimal.Parse(veerud[6]),
                            VatPercent = decimal.Parse(veerud[7]),
                            Summa = decimal.Parse(veerud[8]),
                            Sula = decimal.Parse(veerud[10]),
                            Kaart = decimal.Parse(veerud[11])

                        });
                        int n = m2.kood;

                        db.SaveChanges(); //Salvestab uue sisu

                        db.Database.Connection.Close();

                    }

                    MessageBox.Show($"loetud ridu {db.Myygid2.Count()}");

                    db.SaveChanges();
                }
                */

            #endregion
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
