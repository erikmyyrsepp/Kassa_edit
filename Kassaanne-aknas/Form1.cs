﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kassaanne_aknas
{
    public partial class Form1 : Form
    {
        DateTime algkp = DateTime.MinValue;
        DateTime loppkp;

        public Form1()
        {
            InitializeComponent();
           
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void Täna_Click(object sender, EventArgs e) //Today's sales only
        {
            SalesClassesDataContext db = new SalesClassesDataContext();
            

            var query =
            from x in db.Myygid2s.ToList()
            where x.aeg.Value.Date == DateTime.Today

            orderby x.aeg
            select x; // new { x.aeg, x.tekst, x.admin, x.Tsekinr, x.Kaubakood, x.Kogus, x.Hind, x.VatPercent, x.Summa, x.Sula, x.Kaart};
            
            this.dataGridView1.DataSource = query.ToList();

            Totals();
           
        }


        private void Kuu_Click(object sender, EventArgs e) //Current month sales only
        {
            SalesClassesDataContext db = new SalesClassesDataContext();
            for (int i = 1; i < 31; i++)
            {
                algkp = DateTime.Today.AddDays(-30 + i);
                if (Equals(DateTime.Today.Month, algkp.Month))
                    break;
            }
            algkp = algkp.Date;
            loppkp = DateTime.Today;

            var query =
            from x in db.Myygid2s.ToList()
            where (x.aeg.Value.Date >= algkp) && (x.aeg.Value.Date <= loppkp)
            orderby x.aeg
            select x; // new { x.aeg, x.tekst, x.admin, x.Tsekinr, x.Kaubakood, x.Kogus, x.Hind, x.VatPercent, x.Summa, x.Sula, x.Kaart };

            this.dataGridView1.DataSource = query.ToList();

            Totals();
            
        }

        private void Aasta_Click(object sender, EventArgs e) //Current year sales only
        {
            SalesClassesDataContext db = new SalesClassesDataContext();
            for (int i = 1; i < 365; i++)
            {
                algkp = DateTime.Today.AddDays(-365 + i);
                if (Equals(DateTime.Today.Year, algkp.Year))
                    break;
            }
            algkp = algkp.Date;
            loppkp = DateTime.Today;

            var query =
            from x in db.Myygid2s.ToList()
            where (x.aeg.Value.Date >= algkp) && (x.aeg.Value.Date <= loppkp)
            orderby x.aeg
            select x; // new { x.aeg, x.tekst, x.admin, x.Tsekinr, x.Kaubakood, x.Kogus, x.Hind, x.VatPercent, x.Summa, x.Sula, x.Kaart };

            this.dataGridView1.DataSource = query.ToList();

            Totals();
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
                DateTime algkp= dateTimePicker1.Value;
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
                DateTime loppkp = dateTimePicker2.Value;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        // Request with datetimePicker + Product code or Employee name
        private void Otsing_Click(object sender, EventArgs e)
        {
            string s = comboBox1.Text;
            SalesClassesDataContext db = new SalesClassesDataContext();

            if (textBox1.Text != "" && s == "Product code")
            {

                var query =
                from x in db.Myygid2s.ToList()
                where (int.Parse(textBox1.Text) == x.Kaubakood)
                        && (x.aeg.Value.Date >= dateTimePicker1.Value)
                        && (x.aeg.Value.Date <= dateTimePicker2.Value)
                orderby x.aeg
                select x; // new { x.aeg, x.tekst, x.admin, x.Tsekinr, x.Kaubakood, x.Kogus, x.Hind, x.VatPercent, x.Summa, x.Sula, x.Kaart };

                this.dataGridView1.DataSource = query.ToList();
            }
            else if (textBox1.Text != "" && s == "Employee")
            {
                var query =
                from x in db.Myygid2s.ToList()
                where (textBox1.Text == x.admin)
                        && (x.aeg.Value.Date >= dateTimePicker1.Value)
                        && (x.aeg.Value.Date <= dateTimePicker2.Value)
                orderby x.aeg
                select x; // new { x.aeg, x.tekst, x.admin, x.Tsekinr, x.Kaubakood, x.Kogus, x.Hind, x.VatPercent, x.Summa, x.Sula, x.Kaart };

                this.dataGridView1.DataSource = query.ToList();
            }
             
            else if (comboBox1.SelectedItem == null)
            {
                //kogu valikperiood, kui detailotsing tühi
                var query =
                from x in db.Myygid2s.ToList()
                where (x.aeg.Value.Date >= dateTimePicker1.Value) && (x.aeg.Value.Date <= dateTimePicker2.Value)
                orderby x.aeg
                select x; // new { x.aeg, x.tekst, x.admin, x.Tsekinr, x.Kaubakood, x.Kogus, x.Hind, x.VatPercent, x.Summa, x.Sula, x.Kaart };

                this.dataGridView1.DataSource = query.ToList();
            }
            else
            {
                RequestErrorNotice.Text = $"Required field empty!";
            }

            Totals();
            
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            SalesClassesDataContext db = new SalesClassesDataContext();
            var query =
            from x in db.Myygid2s.ToList().Where(x => false)
            select x; //new { x.aeg, x.tekst, x.admin, x.Tsekinr, x.Kaubakood, x.Kogus, x.Hind, x.VatPercent, x.Summa, x.Sula, x.Kaart };
            this.dataGridView1.DataSource = query.ToList();
        }
        private void Totals()
        {
            //Cash payments total
            decimal sumCash = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                sumCash += Convert.ToDecimal(dataGridView1.Rows[i].Cells[9].Value);
            }
            boxCash.Text = $" {sumCash.ToString()} ";

            //Card payments total
            decimal sumCard = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                sumCard += Convert.ToDecimal(dataGridView1.Rows[i].Cells[10].Value);
            }
            boxCard.Text = $" {sumCard.ToString()} ";

            //Cash+card total
            decimal sumTotal = 0;
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                sumTotal += Convert.ToDecimal(dataGridView1.Rows[i].Cells[8].Value);
            }
            boxTotalAll.Text = $" {sumTotal.ToString()} ";
        }
    }
}
