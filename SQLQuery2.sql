﻿SELECT * FROM sys.databases
backup DATABASE [C:\USERS\ERIKM\SOURCE\REPOS\KASSAANNE-AKNAS\KASSAANNE-AKNAS\BIN\DEBUG\SALESDB.MDF]
TO DISK = 'c:\test\backup.bak'

RESTORE DATABASE Salesdb

FROM DISK = 'c:\test\backup.bak'
WITH MOVE 'SalesDB' TO 'c:\test\salesdb.mdf',
MOVE 'SalesDB_log' TO 'c:\test\salesdb.ldf'
